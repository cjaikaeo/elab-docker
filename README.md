# E-Labsheet Docker

Docker compose configuration for running E-Labsheet system in Docker.

## Building and Running E-Labsheet

1. Clone the elabsheet submodule:

   ```
   git submodule update --init --recursive --remote
   ```

2. Copy the file `env.example` to `.env` and modify the settings.

3. Build the Docker image by running the command:

   ```
   docker compose build web
   ```

4. Prepare secrets for various parts of the system by editing .txt files inside
   the `secrets` directory.  Check `secrets/README.md` for more details.

5. Start all the containers with:

   ```
   docker compose up -d
   ```

6. Add the following lines into your Nginx's `server` block.  The path prefix
   specified for the `location` directives must match the `ELAB_SITE_PREFIX`
   setting in the `.env` file.  The port used by the `proxy_pass` entry also
   has to match the `EXPOSED_PORT` setting in the `.env` file.

   ```
   location = /elab {
       return 302 /elab/;
   }
   location /elab/ {
       proxy_set_header X-Forwarded-Host $host;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_pass http://localhost:8888/;
       proxy_redirect off;
   }
   ```

7. Restart Nginx, then set your browser to `http://<your-host>/elab/`.
