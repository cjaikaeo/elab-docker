# The `secrets` Directory

This directory is supposed to store all the secrets used by E-Labsheet and its
related services.  The .txt files in this directory will be copied into the
`secrets` volume which is mounted and read by containers.  The files inside the
containers will be placed in the `/run/secrets/` directory and set to be only
readable by appropriate accounts.

The following files must be prepared before starting the docker compose services.

* `elab_admin_password.txt` contains the admin's password for E-Labsheet's admin page.
* `elab_kusso_client_id.txt` (optional) contains client ID for the KU-SSO service.
* `elab_kusso_client_secret.txt` (optional) contains client secret for the KU-SSO service.
* `mysql_elab_password.txt` contains the elab's password to MySQL database.
* `mysql_root_password.txt` contains the root's password to MySQL database.
