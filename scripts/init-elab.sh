#!/bin/sh

HOME_DIR=/home/elab
ELAB_DIR=${HOME_DIR}/app

cd ${ELAB_DIR}
python3 manage.py collectstatic_js_reverse
python3 manage.py collectstatic --noinput

cp -a /mathjax /static/

mkdir -p ${ELAB_DIR}/public/media
chown elab:elab ${ELAB_DIR}/public/media

ELAB_ADMIN_PASSWORD=`cat /run/secrets/elab_admin_password.txt`
cat > ${ELAB_DIR}/create_admin.py << EOF
from django.contrib.auth.models import User

name_parts = "${ELAB_ADMIN_NAME}".split(" ", 1)
if len(name_parts) == 2:
    first_name, last_name = name_parts
else:
    first_name = name_parts[0]
    last_name = ""

user, created = User.objects.get_or_create(username="${ELAB_ADMIN_USERNAME}")
user.first_name = first_name
user.last_name = last_name
user.is_superuser = True
user.is_staff = True
user.email = "${ELAB_ADMIN_EMAIL}"
user.set_password("${ELAB_ADMIN_PASSWORD}")
user.save()
EOF
chown elab:elab ${ELAB_DIR}/create_admin.py

su -m -c "
  cd ${ELAB_DIR}
  python3 manage.py migrate
  python3 manage.py shell -c 'import create_admin'
  rm create_admin.py
" elab
