cp -a /secrets/elab_* /run/secrets/
cp -a /secrets/mysql_elab_password.txt /run/secrets/

# do not overwrite existing django secret
if [ ! -f /run/secrets/django_secret_key.txt ]; then
    python3 /scripts/gen-django-secret.py > /run/secrets/django_secret_key.txt
fi

chmod 0400 /run/secrets/*
chown elab:elab /run/secrets/*
