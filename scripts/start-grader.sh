#!/bin/sh

HOME_DIR=/home/elab
ELAB_DIR=${HOME_DIR}/app
PATH=$PATH:${ELAB_DIR}/bin

cd ${ELAB_DIR}
exec python3 manage.py run_grader
