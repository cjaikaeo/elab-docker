#!/bin/sh

HOME_DIR=/home/elab
ELAB_DIR=${HOME_DIR}/app
LOG_DIR=${HOME_DIR}/log
PATH=$PATH:${ELAB_DIR}/bin

cd ${ELAB_DIR}
exec gunicorn elabsheet.wsgi \
  -b 0.0.0.0:8000 \
  --name elab \
  --workers $ELAB_NUM_WEB_WORKERS \
  --access-logfile - \
  --error-logfile -
