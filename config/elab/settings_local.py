import os

def read_secret(name):
    with open(f'/run/secrets/{name}') as f:
        return f.read().strip()


BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = False
SEPARATE_GRADING = True
USE_BOX_IN_SANDBOX = True

ALLOWED_HOSTS = [os.environ['ELAB_HOSTNAME']]
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

SITE_NAME = os.environ['ELAB_SITE_NAME']
SITE_PREFIX = os.environ['ELAB_SITE_PREFIX']
FORCE_SCRIPT_NAME = SITE_PREFIX

SECRET_KEY = read_secret('django_secret_key.txt')
SESSION_COOKIE_PATH = SITE_PREFIX + "/"
SESSION_COOKIE_NAME = 'sessionid_elab'
CSRF_COOKIE_PATH = SITE_PREFIX + '/'

TASKPADS_ENABLED = (os.environ['ELAB_TASKPADS_ENABLED'] == '1')

GRADER_SLEEP_INTERVAL = float(os.environ['ELAB_GRADER_SLEEP_INTERVAL'])

ADMINS = (
    (os.environ['ELAB_ADMIN_NAME'], os.environ['ELAB_ADMIN_EMAIL'])
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'elab',
        'USER': 'elab',
        'PASSWORD': read_secret('mysql_elab_password.txt'),
        'OPTIONS': {
            'autocommit': True,
            'charset': 'utf8mb4',
        },
        'HOST': 'db',   # Set to empty string for localhost. Not used with sqlite3.
        'PORT': 3306,   # Set to empty string for default. Not used with sqlite3.
    },
}

STATIC_URL = f'{SITE_PREFIX}/static/'
STATIC_ROOT = '/static'

MEDIA_URL = f'{SITE_PREFIX}/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'public', 'media')

MATHJAX_ROOT_URL = f'{SITE_PREFIX}/mathjax'
MATHJAX_ROOT_DIR = '/static/mathjax'

LOG_SUBMISSION_ACCESS = (os.environ['ELAB_LOG_SUBMISSION_ACCESS']=='1')

HTTPS_LOGIN = (os.environ['ELAB_HTTPS_LOGIN']=='1')

LOGIN_URL = SITE_PREFIX + '/accounts/login'
LOGIN_REDIRECT_URL = SITE_PREFIX + '/'
LOGOUT_REDIRECT_URL = SITE_PREFIX + '/'

GRADER_OUTPUT_LOG = (os.environ['ELAB_GRADER_OUTPUT_LOG']=='1')

SESSION_EXPIRE_AT_BROWSER_CLOSE = (os.environ['ELAB_SESSION_EXPIRE_AT_BROWSER_CLOSE']=='1')

BUILDERS = {
    #'python' : '/usr/local/bin/python2.7',
    'python3' : '/opt/python-3.12.6/bin/python3',
}

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

if os.environ['ELAB_USE_KUSSO'] == '1':
    KUSSO_CONFIG = {
        'client_id': read_secret('elab_kusso_client_id.txt'),
        'client_secret': read_secret('elab_kusso_client_secret.txt'),
        'create_new_user': (os.environ['ELAB_KUSSO_CREATE_NEW_USER']=='1'),
    }
